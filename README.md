## Integre swagger-php com Slim Framework 3

Este código integra a biblioteca [swagger-php](https://github.com/zircote/swagger-php) com [Slim Framework 3](slimframework.com),
disponibilizando dois recursos:

**1 -**  **JSON FILE:** Geração automática do arquivo json para [Swagger](https://swagger.io/). 
* Rota para o json gerado automaticamente:  
    **[GET]** `/docs/json` *(Pode ser alterada)*
* As informações do json são obtidas por meio de
    [Annotations](https://github.com/zircote/swagger-php/blob/master/docs/Getting-started.md), 
    que são comentários especiais escritos em qualquer arquivo do seu projeto.
* Pode acessar a documentação do [swagger-php por aqui](http://zircote.com/swagger-php/).
* Veja [exemplos](https://github.com/zircote/swagger-php/tree/master/Examples) de uso do Annotations do swagger-php.
* [Getting-started](https://github.com/zircote/swagger-php/blob/master/docs/Getting-started.md) do swagger-php.
* Escaneia por padrão a pasta `/src/` *(Pode ser alterado)*
* `TODO:` Ainda falta a implementação de alguma técnica de  
cacheamento para que não seja escaneado toda vez que a documentação  
é acessada...
    
**2 -** **DOC VIEWER:** Geração de link para visualização da documentação usando o swagger-ui.
* Rota para a visualização da documentação gerada:  
    **[GET]** `/docs/view` *(Pode ser alterada)* 
* Usa os arquivos copiados do projeto [swagger.ui](https://github.com/swagger-api/swagger-ui).
* Links dinamicos são criados para os recursos de js e cs que estão encorporados  
    nesta biblioteca.  
    *(Por isso não precisa se preocupar em copiar para para a pasta public)*
     
    


### Instalação:
**1 -** Este é o único código que precisa adicionar ao arquivo de dependencias do Slim Framework 3 do seu projeto para que a integração funcione.  

<!-- language: PHP -->
    // Configurações da integração Slim+Swagger-PHP e do Swagger-PHP 
    // $app é a variavel que contem a instancia da aplicação
    $container[\JunioDeAlmeida\Slim\SlimSwaggerRouteJav::class] = function ($c) use ($app) {
        return new \JunioDeAlmeida\Slim\SlimSwaggerRouteJav($app);
    };
    // Seteando as rotas /docs/view e /docs/json
    $container[\JunioDeAlmeida\Slim\SlimSwaggerRouteJav::class]->setRouters();
Geralmente este arquivo se encontra em ```/src/dependencies.php``` e possui este código `$container = $app->getContainer();`.

**2 -** Confira no arquivo `/public/index.php` se o nome da variavel que recebe a instancia da aplicação \Slim\App chama `$app` :
<!-- language: PHP -->
    session_start();
    
    // Instantiate the app
    $settings = require __DIR__ . '/../src/settings.php';
    $app = new \Slim\App($settings);
    
    // Set up dependencies
    require __DIR__ . '/../src/dependencies.php';
Caso não seja, adapte este trecho do código: `function ($c) use ($app) {` em:
<!-- language: PHP -->
    $container[\JunioDeAlmeida\Slim\SlimSwaggerRouteJav::class] = function ($c) use ($app) {


**3 -** Adicione nos `settings.php` e altere para que aponte para a pasta raiz onde terão as suas classes com Annotations que serão escaneadas.
<!-- language: PHP -->
    return [
        'settings' => [
    
            // ... here another settings
    
            // Configuração do Swagger - gerador de documentação de api
            'swagger' => [
                // Pasta raiz que será escaneada em busca de Annotations. Um exemplo é chamado se a url não for configurada.
                'baseDir' => __DIR__ . '/../vendor/junioalmeida/slim-framework-swagger-json-and-viewer/Examples/petstore.swagger.io', // Link de exemplo
            //  'baseDir' => __DIR__ . '/../src', // Geralmente as classes do projeto fica na pasta \src\
            //  'ignoreDir' => [],
            //  'routes' => [
            //    'json' => '/docs/json',
            //    'view' => '/docs/view',
            //    'resources' => '/docs/resources/{resource}',
            //  ],
            //  'projects' => [
            //    ['name'=>'This project', 'url'=>'/docs/json'],
            //    ['name'=>'Exemplo', 'url'=>'http://petstore.swagger.io/v2/swagger.json'],
            //  ],
            //]
        ],
    ];
Poderá deixar o exemplo seteado temporariamente para que veja como é a documentação.