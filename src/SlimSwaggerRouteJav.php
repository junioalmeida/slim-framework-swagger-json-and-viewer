<?php
namespace JunioDeAlmeida\Slim;

/**
 * Class SlimSwaggerRouteJav configura Swagger para funcionar automaticamenteo dentro do Slim Framework 3
 *
 * @package JunioDeAlmeida
 */
class SlimSwaggerRouteJav
{
    /**
     * @var array
     */
    protected $options;

    /**
     * @var \Slim\App
     */
    protected $app;

    /**
     * @var \Slim\Container
     */
    protected $container;

    /**
     * Constructor
     * @param \Slim\App $app
     * @param array $options
     */
    public function __construct(\Slim\App $app)
    {
        // Setter of the Slim Container
        $this->app = $app;
        $this->container = $this->app->getContainer();

        $options = (isset($this->container->settings['swagger'])) ? $this->container->settings['swagger'] : [];

        // Setters of the options
        $this->options = array_replace_recursive([
            'baseDir' => __DIR__ . '/../Examples/petstore.swagger.io',
            'ignoreDir' => [],
            'routes' => [
                'json' => '/docs/json',
                'view' => '/docs/view',
                'resources' => '/docs/resources/{resource}'
            ]
        ], $options);

    }

    /**
     * Setea as rotas
     */
    public function setRouters(){
        $this->app->get($this->options['routes']['json'], __CLASS__.':docsJson')->setName('docsJson');
        $this->app->get($this->options['routes']['view'], __CLASS__.':docsView')->setName('docsView');
        $this->app->get($this->options['routes']['resources'], __CLASS__.':getResource')->setName('docsResources');
    }

    /**
     * Mostra o json no formato Swagger, pronto para ser usado
     *
     * TODO: Precisa fazer alguma logica de cache para não escanear todas as vezes
     */
    public function docsJson()
    {
        echo \Swagger\scan($this->options['baseDir'], $this->options['ignoreDir']);
    }

    /**
     * Visualiza a documentação
     *
     * @param $request
     * @param $response
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function docsView($request, $response)
    {
        // verificando se a pasta cache dentro desta biblioteca possui permissão de escrita para cache de template
        $cache = __DIR__.'/cache';
        $cache = ( is_writable($cache) ) ? $cache : false;

        // Recuperando o instancia de Twig
        $view = new \Slim\Views\Twig(__DIR__.'/templates', [
            'cache' => $cache
        ]);

        if( isset($this->options['projects']) ){
            $arrUrlsJson = $this->options['projects'];
        }else{
            $arrUrlsJson[0] = ['name'=>'This Project','url'=>$this->container->router->pathFor('docsJson')];
        }

        $urlSources = $this->container->router->getNamedRoute('docsResources')->getPattern();
        $urlSources = explode('/',$urlSources);
        array_pop($urlSources);
        // Rederizando o template
        return $view->render($response, 'swagger-ui.twig', [
            'urlsJson'=>$arrUrlsJson,
            'docsResources'=>implode('/',$urlSources)
        ]);
    }

    /**
     * Obtem os recursos desta library e disponibiliza em rotas do projeto
     *
     * @param \Slim\Http\Request $request
     * @param \Slim\Http\Response $response
     * @return bool|string
     */
    public function getResource(\Slim\Http\Request $request, \Slim\Http\Response $response){
        $file = explode('-',$request->getAttribute('resource'));
        $ext = array_pop($file);
        $response->write(file_get_contents(__DIR__.'/resources/'.implode('-',$file).'.'.$ext));
        if( $ext=='js' ){
            header('Content-Type: text/javascript');
            $response->withAddedHeader('Content-Type','text/javascript');
            $response->withHeader('Content-Type','text/javascript');
        }elseif( $ext=='css' ){
            $response->withAddedHeader('Content-Type','text/css');
            $response->withHeader('Content-Type','text/css');
            header('Content-Type: text/css');
        }elseif( $ext=='png' ){
            header('Content-Type: image/png');
            $response->withAddedHeader('Content-Type','image/png');
            $response->withHeader('Content-Type','image/png');
        }
        return $response;
    }
}
